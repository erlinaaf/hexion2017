<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'auth'], function()
{
    Route::post('/profile/comment','ProfileController@comment');
	Route::resource('/profile','ProfileController');

	Route::post('{id}/friend','ProfileController@addFriend');
	Route::patch('{id}/accept','ProfileController@acceptFriend');
	Route::post('{id}/game','ProfileController@addGame');
	Route::post('{id}/achievement','ProfileController@addAchievement');
	Route::post('{id}/skill','ProfileController@addSkill');
	Route::get('/home','TimelineController@index');
});
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/',function(){
	return view('homepage');
});


