<?php

use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'title' => 'Dota 2',
            'year' => 2013,
            'price' => 'FREE',
            'description' => str_random(100),
        ]);
        DB::table('games')->insert([
            'title' => 'CS:GO',
            'year' => 2012,
            'price' => '$15',
            'description' => str_random(100),
        ]);
        DB::table('games')->insert([
            'title' => "PUBG(PlayerUnknown's Battlegrounds)",
            'year' => 2017,
            'price' => '$30',
            'description' => str_random(100),
        ]);
        DB::table('games')->insert([
            'title' => 'Pro Evolution Soccer 2018',
            'year' => 2017,
            'price' => '60$',
            'description' => str_random(100),
        ]);
        DB::table('games')->insert([
            'title' => 'Mobile Legends:Bang Bang',
            'year' => 2016,
            'price' => 'FREE',
            'description' => str_random(100),
        ]);
    }
}
