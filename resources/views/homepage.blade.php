<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
	<style>
		@font-face
		{
			font-family: weblysleekuisl;
			src: url("fonts/weblysleekuisl.ttf");
		}

		@font-face
		{
			font-family: weblysleekuisb;
			src: url("fonts/weblysleekuisb.ttf");
		}

		@font-face
		{
			font-family: weblysleekuil;
			src: url("fonts/weblysleekuil.ttf");
		}
		
		body{
			background-image: url('images/home.jpg');
			background-size: cover;
			height: 759px;
			color: white;
			font-family: weblysleekuil;
		}
		.navbar-brand{
			width: 200px;
			height: 70px;
		}
		.navbar{
			background-color: rgba(23,21,36,.5);
			border-style: none;
		}
		.bottom {
		   position:fixed;
		   left:0px;
		   bottom:0px;
		   height:200px;
		   width:100%;
		}
		.top{
			position: fixed;
			left: 0px;
			bottom: 0px;
			height: 600px;
			width: 100%;
		}
		.meet{
			font-size: 50px;
		}
		.tagline{
			font-size: 30px;
		}
		.login{
			opacity: 0.5;
		}
		.login:hover{
			opacity: 1;
		}
		.tag{
			position: fixed;
			left: 0px;
			bottom: 0px;
			height: 660px;
			width: 100%;
		}
		.btn{
			background-color: #92393d;
			opacity: 0.8;
			border-radius: 0;
			font-size: 20px
		}
		.btn:hover{
			opacity: 1;
		}
		.glitch_word_box {			
			position: relative;		
			-webkit-animation: disappear 1s linear;
			-webkit-animation-iteration-count: infinite, infinite;
		}
					
		.glitch_word_box .glitch_word0 {
			position: absolute;
			width: 50%;
		}
					
		.glitch_word_box .glitch_word1 {
			
			width: 50%;	
			-webkit-animation: animate_glitch_1 .2s linear;
			-webkit-animation-iteration-count: infinite;	
		}
					
		.glitch_word_box .glitch_word2 {
			position: absolute;
			transform: translateX(50%);
			width: 50%;
			-webkit-animation: animate_glitch_2 .3s linear;
			-webkit-animation-iteration-count: infinite;
		}
					
		@-webkit-keyframes disappear {
			0% {	opacity: 0;	}
			2% { opacity: 1; }
		}
								
		@-webkit-keyframes animate_glitch_1 {
			0% { top: 1px; left: 1px; }
			25% { top: 3px; left: 2px;	 }
			50% { top: -1px; left: -4px;	}
			75% { top: 2px; left: 5px;	}
			100% {	top: 1px; left: -2px;	}			
		}
					
		@-webkit-keyframes animate_glitch_2 {
			0% { top: -1px; left: -1px;	}
		 	25% { top: -3px; left: -2px;	}
		 	50% { top: 1px; left: 4px; }
			100% { top: -1px; left: -1px; }			
		}
		@media screen and (max-width: 940px){
			.glitch_word_box .glitch_word0{

				width: 60%;
			}
			.glitch_word_box .glitch_word1{

				width: 60%;
			}
			.glitch_word_box .glitch_word2{
				margin-left: -10%;
				width: 60%;
			}
		}
		@media screen and (max-width: 770px){
			.glitch_word_box .glitch_word0{

				width: 70%;
			}
			.glitch_word_box .glitch_word1{

				width: 70%;
			}
			.glitch_word_box .glitch_word2{
				margin-left: -20%;
				width: 70%;
			}
		}
		@media screen and (max-width: 670px){
			.glitch_word_box .glitch_word0{

				width: 80%;
			}
			.glitch_word_box .glitch_word1{

				width: 80%;
			}
			.glitch_word_box .glitch_word2{
				margin-left: -30%;
				width: 80%;
			}
		}
		@media screen and (max-width: 570px){
			.glitch_word_box .glitch_word0{

				width: 90%;
			}
			.glitch_word_box .glitch_word1{

				width: 90%;
			}
			.glitch_word_box .glitch_word2{
				margin-left: -40%;
				width: 90%;
			}
		}
		@media screen and (max-width: 500px){
			.glitch_word_box .glitch_word0{

				width: 100%;
			}
			.glitch_word_box .glitch_word1{

				width: 100%;
			}
			.glitch_word_box .glitch_word2{
				margin-left: -50%;
				width: 100%;
			}
			h3{
				font-size: 20px;
			}
		}
	</style>
</head>
<body style="margin: 0;">
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<img class="navbar-brand" href="#" src="{{url('images/gamehub.png')}}">
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{url('login')}}" class="login" style="font-size: 20px;margin-top: 6px;color: white;" href="#">Login</a></li>
					<li><a href="{{url('register')}}" class="login" style="font-size: 20px;margin-top: 6px;color: white;" href="#">Register</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
	<div>
		<center>
			<div class="tag">
				<span class="meet">meet</span><br>
			</div>
				
			<div class="top">
				<div class="glitch_word_box">
					<div class="line"></div>
					<img class="glitch_word0" src="{{url('images/gamehub.png')}}">
					<img class="glitch_word1" src="{{url('images/gamehub.png')}}">
					<img class="glitch_word2" src="{{url('images/gamehub.png')}}">
				</div>
				<span class="tagline" style="margin-top: 2%">where all <strong>gamers&nbsp</strong><br>
				<strong id="changeText"></strong></span>
				
			</div>
			<div class="bottom" style="margin-top: 15%">
				<h2>start <strong>connecting</strong> now</h2>
				<a href="{{url('register')}}" type="button" class="btn btn-danger">Sign Up</a>
			</div>
		</center>
	</div>
	<script src="{{'js/jquery.min.js'}}"></script>
	<script>
		$(function () {
		  count = 0;
		  wordsArray = ["connect", "collaborate","communicate"];
		  setInterval(function () {
		    count++;
		    $("#changeText").fadeOut(500, function () {
		      $(this).text(wordsArray[count % wordsArray.length]).fadeIn(500);
		    });
		  },1200);
		});
	</script>
</body>
</html>