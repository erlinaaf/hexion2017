<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet">
    <style>
        .modal-pict{
            width: 150px;
            height: 150px; 
            background-color: white;
            display: block;
            margin: auto;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .nav-tabs{
            border:none;
        }
        .tab-nav{
            color:white;
        }
        .active a{
            background-color: #3d3041 !important;
            border:none !important;
            color: white !important;
        }
        .nav-tabs li a:hover{
            background: #3d3041 !important;
            border-color: #3d3041 !important;
        }
        .slider {
            width: 75%;
            margin: 100px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="navbar-brand" href="#" src="{{url('images/gamehub.png')}}">
            </div>
        
        <div class="collapse navbar-collapse navbar-ex1-collapse">  
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/gamepedia.png')}}"><br>Gamepedia</a></li>
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/message.png')}}"><br>Message</a></li>
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/notification.png')}}"><br>Notification</a></li>
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/friendrequest.png')}}"><br>Friend Request</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="navicon center-block" src="{{asset('images/user.png')}}"><br>{{Auth::user()->name}}</a>
                <ul class="dropdown-menu">
                    <li><a style="color: black" href="#"></a>View Profile</li>
                    <li><a style="color: black" href="#"></a>Settings</li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
                </li>
            </ul>
            </div>
        </div>
    </nav>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0; padding: 0;">
        <div id="gradient" class="container-fluid pro" style="margin: 0; padding: 0;">
            <div class="container profile">
                <div class="col-xs-6 col-lg-4">
                    @if(!($user->image))
                    <img style="" class="img-circle pict" src="{{url('images/profile.png')}}">
                    @else
                    <img class="img-circle pict" src="{{asset('../resources/assets/images/'.Auth::user()->image)}}">
                    @endif    
                </div>
                <div class="col-xs-6 col-lg-8">
                    <h3>{{Auth::user()->name}} <br>
                    
                    <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-danger" style="margin-top: 20px;">Edit Profile</button></h3>
                    <div class="container" style="padding: 0px; margin: 0px;">
                        <div class="col-xs-12 col-lg-3 atr" style="padding: 0px;">
                            <h4><b>{{$cfriends}}</b></h4>
                            <h5>Friends</h5>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 atr2" style="padding: 0px; margin-top: 20px;">
                            <h4><b>{{$cgames}}</b></h4>
                            <h5>Games</h5>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 atr2" style="padding: 0px; margin-top: 20px;">
                            <h4><b>{{$cachievements}}</b></h4>
                            <h5>Achievement</h5>
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3 atr2" style="padding: 0px; margin-top: 20px;">
                            <h4><b>{{$cskills}}</b></h4>
                            <h5>Skills</h5>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content" style="background-color: #322f3e;margin: 0;">
                            <div class="modal-header text-center" style="border: none;padding-bottom: 0">
                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                   <a href="" data-dismiss="modal" style="color: white;text-decoration: none"></a>
                              </div>
                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                   <h4 class="modal-title">Edit Profile</h4>
                              </div>
                              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                   <a href="" data-dismiss="modal-title" style="color: white;text-decoration: none">Cancel</a>
                              </div>

                            </div>
                            <div class="container" style="margin-bottom: 10px;">
                                @if(!($user->image))
                                <img class="img-circle modal-pict" src="{{url('images/profile.png')}}">
                                @else
                                <img class="img-circle pict" src="{{asset('../resources/assets/images/'.Auth::user()->image)}}">
                                @endif
                            </div>

                        <ul class="nav nav-tabs" style="background-color: #322f3e">
                            <li class="active"><a class="tab-nav" data-toggle="tab" href="#home"><img src="{{url('images/user.png')}}"></a></li>
                            <li><a class="tab-nav" data-toggle="tab" href="#menu1"><img src="{{url('images/friend.png')}}"></a></li>
                            <li><a class="tab-nav" data-toggle="tab" href="#menu2"><img src="{{url('images/gamepedia.png')}}"></a></li>
                            <li><a class="tab-nav" data-toggle="tab" href="#menu3"><img src="{{url('images/quality.png')}}"></a></li>
                            <li><a class="tab-nav" data-toggle="tab" href="#menu4"><img src="{{url('images/knight.png')}}"></a></li>
                        </ul>

                        
                        <div class="modal-body" style="background-color: #3d3041;">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <form action="{{url('/profile/'.Auth::user()->id)}}" method="POST" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="_method" value="PATCH">
                                    {{csrf_field()}}
                                    <div class="col-lg-12">
                                        <h4 style="padding-left: 15px;">Personal Information</h4>
                                        <div class="col-lg-4 text-left">
                                            <h5>Name</h5>
                                        </div>
                                        <div class="col-lg-8">
                                            <input style="height: 30px;color: white;" type="text" name="fname" id="input" class="form-control" value="{{Auth::user()->fname}}" title="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <h5>Username</h5>
                                        </div>
                                        <div class="col-lg-8">
                                            <input style="height: 30px;color: white;" type="text" name="name" id="input" class="form-control" value="{{Auth::user()->name}}" title="">
                                        </div>
                                     </div>

                                    <div class="col-lg-12">
                                        <h4 style="padding-left: 15px;">Private Information</h4>
                                        <div class="col-lg-4">
                                            <h5>Birthday</h5>
                                        </div>
                                        <div class="col-lg-8">
                                            <input style="height: 30px;color: white;" type="date" name="birthday" id="input" class="form-control" value="{{Auth::user()->birthday}}" title="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <h5>Gender</h5>
                                        </div>
                                        <div class="col-lg-8">
                                            <input style="height: 30px;color: white;" type="text" name="gender" id="input" class="form-control" value="{{Auth::user()->gender}}" title="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <h5>Current Country</h5>
                                        </div>
                                        <div class="col-lg-8">
                                            <input style="height: 30px;color: white;" type="text" name="country" id="input" class="form-control" value="{{Auth::user()->country}}" title="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <h5>Current City</h5>
                                        </div>
                                        <div class="col-lg-8">
                                            <input style="height: 30px;color: white;" type="text" name="city" id="input" class="form-control" value="{{Auth::user()->city}}" title="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h4 style="padding-left: 15px;">Description</h4>
                                        <textarea style="margin-left: 15px;margin-right: 15px;color: white;" name="description" id="input" class="form-control" rows="3" required="required">{{Auth::user()->description}}</textarea>
                                        <button style="float:right;margin-right: 15px;" type="submit" class="btn btn-success">Update</button>
                                    </div>

                                    
                                </form>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    <div class="col-lg-12">
                                        <h4 style="padding-left: 15px;">Friends</h4>
                                        @foreach($friends as $friend)
                                            <h6>{{$friend->name}}</h6>
                                        @endforeach
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade">
                                    <div class="col-lg-12" style="padding-left: 30px">
                                        <h4>Games</h4>
                                        <h5>Your Games</h5>
                                        @foreach($games as $game)
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <h6>{{$game->title}}</h6>

                                        </div>
                                        @endforeach
                                        @if($cgames == 0)
                                            <h6><b>No Games Yet</b></h6>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <form action="{{url(Auth::user()->name.'/game')}}" method="POST" role="form">
                                            <h5 for="">Add Games</h5>
                                            {{csrf_field()}}        
                                            <div class="form-group">
                                                
                                                <select name="games_id" id="input" class="form-control" required="required" style="height: 30px">
                                                    @foreach($lgames as $lgame)
                                                        <option value="{{$lgame->id}}">{{$lgame->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </form>
                                    </div>
                                </div>
                                <div id="menu3" class="tab-pane fade">
                                    <div class="col-lg-12" style="padding-left: 15px;>
                                        <h4">Achievements</h4>
                                        <h5>Your Achievement</h5>
                                        @foreach($achievements as $achievement)
                                        <div class="col-xs-4 col-sm-4 col-md-4">
                                            <h6>{{$achievement->achievement}}</h6>

                                        </div>
                                        @endforeach
                                        @if($cachievements == 0)
                                            <h6><b>No Achievements Yet</b></h6>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <form action="{{url(Auth::user()->name.'/achievement')}}" method="POST" role="form">

                                            <h5 for="">Add Achievement</h5>
                                            {{csrf_field()}}        
                                            <div class="form-group">
                                                <label>Name of Achievement</label>
                                                <input type="text" name="achievement" id="input" class="form-control" value="" required="required" title="" style="height: 30px;">
                                            </div>
                                            <div class="form-group">
                                                <label>Year of Achievement</label>
                                                <input type="text" name="year" id="input" class="form-control" value="" required="required" title="" style="height: 30px;">
                                            </div>

                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </form>
                                    </div>
                                </div>
                                <div id="menu4" class="tab-pane fade">
                                    <div class="col-lg-12">
                                        <h4 style="padding-left: 15px;">Skills</h4>
                                        <h5>Your Skill</h5>
                                        @foreach($skills as $skill)
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <h6>{{$skill->skill}}</h6>
                                            </div>
                                        @endforeach
                                        @if($cskills == 0)
                                            <h6><b>No Skills Yet</b></h6>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <form action="{{url(Auth::user()->name.'/skill')}}" method="POST" role="form">
                                            <h5 for="">Add Achievement</h5>
                                            {{csrf_field()}}        
                                            <div class="form-group">
                                                <label>Name of Skill</label>
                                                <input type="text" name="skill" id="input" class="form-control" value="" required="required" title="" style="height: 30px;">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </form>
                                    </div>
                                </div>
                              </div>    
                            <span style="color:#3d3041 ">asd</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
        <div class="container post post2">
            <form action="" method="POST" role="form">
                <div class="form-group">
                    <input type="text" class="form-control" style="margin-top: 0px;" id="text" placeholder= "what's on your mind?">
                </div>
                <img src="images/gallery.png"><span>&nbsp<img src="images/film-strip.png"></span><button type="post" class="btn btn-danger" style="font-size: 1em; float: right;">Post</button>
            </form>
        </div>
`       <?php $count = 0; ?>
        <div class="container">
            <h2 >List of Games</h2>
            <div class="container" style="padding: 0; margin: 0;">  
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    @foreach($games as $game)
                        @if($count < 3)
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <img class="img-circle list center-block">
                                <h6>{{$game->title}}</h6>
                            </div>
                        @endif
                    <?php $count++; ?>
                    @endforeach
                    @if($cgames == 0)
                        <h6><b>No Games Yet</b></h6>
                    @endif

                </div>
            </div>
            @if($cgames > 3)
                <ul>
                    <li><a data-toggle="modal" data-target="#myModal" href="" style="vertical-align: center; color: white;">View All</a></li>
                </ul>
            @endif
            <hr>
        </div>
        <?php $countac = 0; ?>
        <div class="container text-center">
            <h2 >Achievement</h2>
            <div class="container" style="padding: 0; margin: 0;">  
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @foreach($achievements as $achievement)
                        @if($countac < 3)
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <img class="img-circle list center-block">
                                <h6>{{$achievement->achievement}}</h6>
                            </div>
                             
                        @endif
                        <?php $countac++; ?>
                    @endforeach
                    @if($cachievements == 0)
                        <h6><b>No Achievements Yet</b></h6>
                    @endif
                   
                </div>
            </div>
            @if($cachievements > 3)
                <ul>
                    <li><a data-toggle="modal" data-target="#myModal" style="vertical-align: center; color: white;">View All</a></li>
                </ul>
            @endif
            <hr>
        </div>
        <?php $countskill = 0; ?>
        <div class="container text-center"  style="padding: 0; margin: 0;">
            <h2 >Skills <b class="caret"></b></h2>
            <div class="container">
                <div class="col-lg-12" style="padding: 0; margin: 0;">
                </div>
                
                @foreach($skills as $skill)
                    @if($countskill < 3)
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <h5>{{$skill->skill}}</h5>
                            <h6>0 likers</h6>
                            <img class="img-responsive center-block" src="images/love.png" style="margin-top: 15px;" id="love-btn">
                        </div>
                        
                    @endif
                    <?php $countskill++; ?>
                @endforeach
                
                @if($cskills == 0)
                    <h6><b>No Skills Yet</b></h6>
                @endif
            </div>
            @if($cskills > 3)
                <ul>
                    <li><a data-toggle="modal" data-target="#myModal" style="vertical-align: center; color: white;">View All</a></li>
                </ul>
            @endif


        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
        <div class="container post post1">
            <form id="frmProducts" name="frmProducts" class="form-horizontal" role="form">
                <div class="form-group">
                    <textarea style="color: white;width: 100%" name="post" id="post-data" class="form-control" rows="2" required="required" placeholder="What's on your mind?"></textarea>
                    <span>
                        <label for="file-input">
                             <img src="images/gallery.png">
                        </label>
                        <input style="display: none" id="file-input" type="file" name="image">
                    </span>
                    <span><img src="images/film-strip.png"></span>
                    <input type="hidden" name="like" value="0">
                    <button type="submit" id="btn-save" class="btn btn-danger" style="font-size: 1em; float: right;">Post</button>
                </div>
            </form>
        </div>
        <div id="post-list"></div>
        @foreach($posts->reverse() as $post)
        <div class="container post post-head{{$post->id}}">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;">
                <div class="col-xs-2 col-sm-2 col-md-2">
                    <img class="img-circle list2 center-block" style="display: block;">
                    <button style="outline:none;background-color: transparent;border: none;display: block;margin: auto;"><img class="img-responsive center-block" src="images/love.png" style="margin-top: 15px;" id="love-btn{{$post->id}}"></button>
                    <h6 id="like-value{{$post->id}}" style="text-align: center">0</h6>
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10" style="padding-left: 0px; padding-right: 0px;">
                    <div class="container-fluid posting">
                        <div class="form-group">
                            <h3 style="font-size: 1.2em; margin-top: 30px; margin-bottom: 30px; text-align: left;">{{Auth::user()->name}}</h3>
                            <h4 id="" style="font-size: 1em; text-align: left;">
                                {{$post->post}}
                            </h4>
                            
                        </div>  
                    </div>
                    
                    @foreach($commented as $comments)
                    @if($comments->post_id == $post->id)
                    <div class="container-fluid" style="margin-top: 20px;">
                        <div class="col-xs-1 col-sm-1 col-md-1" style="padding:0px">
                            <img class="img-circle list2 center-block" style="display: block;margin:0;">
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10" style="margin-left: 28px;padding-right: 0px;margin-top: 5px;">
                                {{$comments->user->name}}<br>
                                {{$comments->comment}}<br>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    
                    <div id="comment-list{{$post->id}}"></div>
                    <div class="container-fluid" style="margin-top: 20px;">
                        <div class="col-xs-1 col-sm-1 col-md-1" style="padding:0px">
                            <img class="img-circle list2 center-block" style="display: block;margin:0;">
                        </div>
                        <form>
                            <div class="col-xs-8 col-sm-9 col-md-9" style="margin-left: 28px;padding-right: 0px;">
                                <div class="contaicommentner posting" style="padding: 0;">
                                    <div class="form-group">
                                        <input type="text" name="comment" id="comment-data{{$post->id}}" class="form-control comment-data" placeholder="Write your comment" required="required" title="" style="height: 50px;margin-left: 0;margin-right: 0px">
                                    </div>  
                                    <input type="hidden" id="post-id{{$post->id}}" style="display:none" value="{{$post->id}}">
                                    <input type="hidden" id="user-id{{$post->id}}" style="display:none" value="{{Auth::user()->id}}">
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1">
                                <button style="background-color: transparent;border: none;float: right;outline: none;padding: 0;" id="comment-post{{$post->id}}" type="submit"><img src="images/chatx.png" class="comment-post" style=" margin-right: 15px; margin-bottom: 15px;margin-top: 20px;"></button>
                                    <script src="{{'js/jquery.min.js'}}"></script>
                                    <script>
                                     $("#comment-post"+{{$post->id}}).click(function (e) {
                                        $.ajaxSetup({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                            }
                                        })
                                        e.preventDefault(); 
                                        var commentData = {
                                            comment : $('#comment-data'+{{$post->id}}).val(),
                                            post_id : $('#post-id'+{{$post->id}}).val(),
                                            user_id : $('#user-id'+{{$post->id}}).val(),
                                        }
                                        var type = "POST";
                                        var my_url = url;
                                        console.log(commentData);

                                        $.ajax({
                                            type: type,
                                            url: my_url+"/comment",
                                            data: commentData,
                                            dataType: 'json',
                                            success: function (data) {
                                                console.log(data);
                                                var comment ='<div class="container-fluid" style="margin-top: 20px;"><div class="col-xs-1 col-sm-1 col-md-1" style="padding:0px"><img class="img-circle list2 center-block" style="display: block;margin:0;"></div><div class="col-xs-10 col-sm-10 col-md-10" style="margin-left: 28px;padding-right: 0px;margin-top: 5px;">{{Auth::user()->name}}<br>'+data.comment+'</div></div>';
                                                $('#comment-list{{$post->id}}').append(comment).hide();
                                                $('#comment-list{{$post->id}}').fadeIn('slow');
                                                $('#comment-data{{$post->id}}').val(" ");
                                            },
                                            error: function (data) {
                                                console.log('Error:', data);
                                            }
                                        });
                                    });
                                    $("#love-btn{{$post->id}}").click(function (e){
                                        var formData = {
                                            like: $('#like-value{{$post->id}}').text(),
                                        }
                                        var count = $('#like-value{{$post->id}}').val();
                                        formData = parseInt(formData.like);
                                        var type = "PUT";
                                        var post_id = $('#post-id').text();
                                        var my_url = url + '/' + post_id;
                                        if(count%2 == 0)
                                            formData = formData + 1;
                                        else
                                            formData = formData - 1;

                                        $('#like-value{{$post->id}}').html(formData);
                                    });
                                </script>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        @if(count($posts) == 0)
            <h6>No Posts Yet</h6>
        @endif
    </div>

    </div>


    <!-- Scripts -->
    <meta name="_token" content="{!! csrf_token() !!}" />
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{'js/background.js'}}"></script>
    <script>

    var url = "http://localhost/hexion/public/profile";
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault(); 
        var formData = {
            post: $('#post-data').val(),
            image: $('#file-input').val(),
            comment: "",
            like: 0,
        }
        
        
        var type = "POST";
        var my_url = url;
        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var post = '<div class="container post"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;"><div class="col-xs-2 col-sm-2 col-md-2"><img class="img-circle list2 center-block" style="display: block;"><button style="outline:none;background-color: transparent;border: none;display: block;margin: auto;"><img class="img-responsive center-block" src="images/love.png" style="margin-top: 15px;" id="love-btn"></button><h6 id="like-value" style="text-align: center">0</h6></div><div class="col-xs-10 col-sm-10 col-md-10"style="padding-left: 0px; padding-right: 0px;"><div class="container posting"><div class="form-group" id="post-list"><h3 style="font-size: 1.2em; margin-top: 30px; margin-bottom: 30px; text-align: left;">{{Auth::user()->name}}</h3><h4 id="post" style="font-size: 1em; text-align: left;">'
                    +data.post+
                    '</h4></div></div><img src="images/chatx.png" style="float: right; margin-right: 15px; margin-bottom: 15px;"></div></div></div>';
                    $('#post-list').prepend(post).hide();
                    $('#post-list').fadeIn('slow');
                    $('#post-data').val(" ");
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
</script>
</body>
</html>
