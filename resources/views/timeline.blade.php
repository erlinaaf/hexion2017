<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/timeline.css')}}">
	<script type="text/javascript"></script>
	<title>Timeline</title>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="navbar-brand" href="#" src="{{url('images/gamehub.png')}}">

            </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse"> 
            <ul class="nav navbar-nav navbar-right text-center">
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/gamepedia.png')}}"><br>Gamepedia</a></li>
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/friendrequest.png')}}"><br>Friend Request</a></li>
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/message.png')}}"><br>Message</a></li>
                <li><a href="#"><img class="navicon center-block" src="{{asset('images/notification.png')}}"><br>Notification</a></li>
                
                <li class="dropdown">
                	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="navicon center-block" src="{{asset('images/user.png')}}" ><br>{{Auth::user()->name}}</a>

           
                	<ul class="dropdown-menu">
	                    <li><a style="color: black" href="{{url('profile')}}">View Profile</a></li>
	                    <li><a style="color: black" href="#">Settings</a></li>
	                    <li>
	                        <a href="{{ route('logout') }}"
	                            onclick="event.preventDefault();
	                            document.getElementById('logout-form').submit();">
	                            Logout
	                        </a>

	                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                            {{ csrf_field() }}
	                        </form>
	                    </li>
                	</ul>
                </li>
            </ul>
        </div>
        </div>
    </nav>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="container profile">
			<div class="col-lg-3">
				<div class="userprofile">
					<img class="profilepict" style="margin-left: 110px;margin-top: 50px">
					<div id="gradient" class="container profile1" style="margin-left: 0px; padding-left: 0px;">
					</div>
					<div class="container profile2" style="margin-left: 0px; padding-left: 0px;">
						<h2 style="text-align: center; vertical-align: text-bottom;">{{Auth::user()->name}}</h2>
					</div>
				</div>		
				
	<div class="container leftside" style="padding-left: 0px; margin-left: 0px;">
		<div class="col-lg-offset-3 col-lg-3">
			<div class="container icon" style="padding-left: 0px; margin-left: 0px;">
				<img class="atr center-block" src="{{asset('images/group.png')}}">	
			</div>
			<div class="container icon" style="padding-left: 0px; margin-left: 0px;">
				<img class="atr center-block" src="{{asset('images/quality.png')}}">
			</div>
			<div class="container icon" style="padding-left: 0px; margin-left: 0px;">
				<img class="atr center-block" src="{{asset('images/gamepedia.png')}}">
			</div>
			<div class="container icon" style="padding-left: 0px; margin-left: 0px;">
				<img class="atr center-block" src="{{asset('images/knight.png')}}">
			</div>
		</div>
		<div class=" col-lg-6">
			<div class="container value" style="padding-left: 0px; margin-left: 0px;">
				<h2>{{$cfriends}}</h2>
			</div>
			<div class="container value" style="padding-left: 0px; margin-left: 0px;">
				<h2>{{$cachievements}}</h2>
			</div>
			<div class="container value" style="padding-left: 0px; margin-left: 0px;">
				<h2>{{$cgames}}</h2>
			</div>
			<div class="container value" style="padding-left: 0px; margin-left: 0px;">
				<h2>{{$cskills}}</h2>
			</div>
		</div>
	</div>
	</div>
			
		<div class="col-lg-6">
			<div class="container post" style="padding-left: 0px; margin-left: 0px;">
				<form action="" method="POST" role="form" enctype="multipart/form-data">
					<div class="form-group">
						{{csrf_field()}}
						<input type="text" class="form-control" style="margin-top: 0px;" id="post-data" placeholder= "what's on your mind?">
						<span>
	                        <label for="file-input">
	                             <img src="{{asset('images/gallery.png')}}">
	                        </label>
	                        <input style="display: none" id="file-input" type="file" name="image">
	                    </span>
						<span>&nbsp<img src="{{asset('images/film-strip.png')}}"></span>
						<button type="post" class="btn btn-danger" id="btn-save" style="font-size: 1em; float: right;">Post</button>
					</div>
				</form>
			</div>
			<div id="post-list"></div>
			@foreach($posts->reverse() as $post)
			<div class="container peoplepost" style="padding-left: 0px; margin-left: 0px;padding-bottom: 20px">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<img class="atr2 center-block">
					<h2 style="text-align: center">0</h2>
					 
					<img class="like center-block" src="{{asset('images/love.png')}}">
				</div>
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
					<h5>{{$post->user->name}}<span style="float: right;"><b style="font-size: 0.8em;">2h</b></span></h5>
					<p>
						{{$post->post}}
					</p>
					<img class="comment" src="images/chatx.png"><span><img class="share" src="{{asset('images/share.png')}}"></span>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px">
					@foreach($commented as $comments)
                    @if($comments->post_id == $post->id)
                    <div class="container-fluid" style="margin-top: 20px;">
                        <div class="col-xs-1 col-sm-1 col-md-1" style="padding:0px">
                            <img class="img-circle list2 center-block" style="display: block;margin:0;">
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-9" style="margin-left: 28px;padding-right: 0px;margin-top: 5px;">
                                {{$comments->user->name}}<br>
                                {{$comments->comment}}<br>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    
                    <div id="comment-list{{$post->id}}"></div>
					
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="">
							<img class="img-circle list2 center-block" style="margin-left: 0px">
						</div>
						<div class="col-xs-8 col-sm-90 col-md-9 col-lg-9">
							<input type="text" name="comment" id="comment-data{{$post->id}}" class="form-control comment-data" placeholder="Write your comment" required="required" title="" style="height: 50px;margin-left: 0;margin-right: 0px">
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<span style="float:right;display: flex"><button style="background-color: transparent;border: none;float: right;outline: none;padding: 0;" id="comment-post{{$post->id}}" type="submit"><img src="images/chatx.png" class="comment-post" style=" margin-right: 15px; margin-bottom: 15px;margin-top: 20px;"></button></span>
							<input type="hidden" id="post-id{{$post->id}}" style="display:none" value="{{$post->id}}">
                            <input type="hidden" id="user-id{{$post->id}}" style="display:none" value="{{Auth::user()->id}}">
						</div>
					</div>
					<script src="{{'js/jquery.min.js'}}"></script>
                   	<script>
                        $("#comment-post"+{{$post->id}}).click(function (e) {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                }
                            })
                            e.preventDefault(); 
                            var commentData = {
                                comment : $('#comment-data'+{{$post->id}}).val(),
                                post_id : $('#post-id'+{{$post->id}}).val(),
                                user_id : $('#user-id'+{{$post->id}}).val(),
                            }
                            var type = "POST";
                            var my_url = url;
                            console.log(commentData);

                            $.ajax({
                                type: type,
                                url: my_url+"/comment",
                                data: commentData,
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                    var comment ='<div class="container-fluid" style="margin-top: 20px;"><div class="col-xs-1 col-sm-1 col-md-1" style="padding:0px"><img class="img-circle list2 center-block" style="display: block;margin:0;"></div><div class="col-xs-10 col-sm-10 col-md-10" style="margin-left: 28px;padding-right: 0px;margin-top: 5px;">{{Auth::user()->name}}<br>'+data.comment+'</div></div>';
                                    $('#comment-list{{$post->id}}').append(comment).hide();
                                    $('#comment-list{{$post->id}}').fadeIn('slow');
                                    $('#comment-data{{$post->id}}').val(" ");
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        });
                        $("#love-btn{{$post->id}}").click(function (e){
                            var formData = {
                                like: $('#like-value{{$post->id}}').text(),
                            }
                            var count = $('#like-value{{$post->id}}').val();
                            formData = parseInt(formData.like);
                            var type = "PUT";
                            var post_id = $('#post-id').text();
                            var my_url = url + '/' + post_id;
                            if(count%2 == 0)
                                formData = formData + 1;
                            else
                                formData = formData - 1;

                            $('#like-value{{$post->id}}').html(formData);
                        });
                    </script>
				</div>
			</div>
			@endforeach

			<div class="container peoplepost" style="padding-left: 0px; margin-left: 0px;padding-bottom: 20px">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
					<img class="atr2 center-block">
					<h2 style="text-align: center">10</h2>
					<img class="like center-block" src="{{asset('images/love.png')}}">
				</div>

				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
					<h5>Username<span style="float: right;"><b style="font-size: 0.8em;">2h</b></span></h5>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
					<img class="contohpost" src="{{asset('images/contohpost.jpg')}}">
					<img class="comment" src="{{asset('images/chatx.png')}}"><span><img class="share" src="{{asset('images/share.png')}}"></span>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<img class="atr2 center-block">
						</div>
						<div class="col-xs-8 col-sm-90 col-md-9 col-lg-9">
							<input type="text" name="comment" id="comment-data" class="form-control comment-data" placeholder="Write your comment" required="required" title="" style="height: 50px;margin-left: 0;margin-right: 0px">
						</div>
						<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
							<span style="float:right;display: flex"><button style="background-color: transparent;border: none;float: right;outline: none;padding: 0;" id="comment-post" type="submit"><img src="images/chatx.png" class="comment-post" style=" margin-right: 15px; margin-bottom: 15px;margin-top: 20px;"></button></span>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="container friendreq" style="padding-left: 0px; margin-left: 0px;">
				<h3>Friend Request</h3>
				@foreach($users as $user)
					@if($user->id != Auth::user()->id)
					<div class="col-lg-12">
						<div class="col-lg-4" style="padding-left: 0px; margin-left: 0px;">
							<img class="img-circle center-block">	
						</div>
						<div class="col-lg-6" style="padding-left: 0px; margin-left: 0px;margin-top: 15px">
							<h6>{{$user->name}}</h6>	
						</div>
						<div class="col-lg-2">
							<form action="{{url($user->id.'/friend')}}" method="POST" role="form">
								{{csrf_field()}}
	                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
	                            <input type="hidden" name="friend_id" value="{{$user->id}}">
								<input type="hidden" name="conf" value=0>
								<button type="submit" style="margin-left: -15px" class="btn btn-danger">Add</button>
							</form>				
						</div>
					</div>
					@endif
				@endforeach

				@foreach($friends as $friend)
					@if($friend->conf == 0)
						@if($friend->friend_id == Auth::user()->id)
							
								<div class="col-lg-12">
								<div class="col-lg-4" style="padding-left: 0px; margin-left: 0px;">
									<img class="img-circle center-block">	
								</div>
								<div class="col-lg-6" style="padding-left: 0px; margin-left: 0px;margin-top: 15px">
									<h6>{{$friend->user->name}}</h6>	
								</div>
								<div class="col-lg-2">
									<form action="{{url($friend->id.'/accept')}}" method="POST" role="form">
										{{csrf_field()}}
    									{{ method_field('PATCH') }}
                                    	<input type="hidden" name="user_id" value="{{$friend->user_id}}">
                                    	<input type="hidden" name="friend_id" value="{{$friend->friend_id}}">
										<input type="hidden" name="conf" value=1>
										<button type="submit" style="margin-left: -15px" class="btn btn-danger">Accept</button>
									</form>
								</div>
							
							</div>
						@endif
					@endif
				@endforeach
			</div>

			<div class="container sponsor" style="padding-left: 0px; margin-left: 0px;">
				<h3>Sponsor</h3>
			</div>
		</div>
		</div>
	</div>
	<meta name="_token" content="{!! csrf_token() !!}" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="{{ asset('js/app.js') }}"></script>
    <script src="{{'js/background.js'}}"></script>
    <script>

    var url = "http://localhost/hexion/public/profile";
    $("#btn-save").click(function (e) {
    	const xCsrfToken = "{{ csrf_token() }}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': xCsrfToken
            }
        })
        e.preventDefault(); 
        var formData = {
            post: $('#post-data').val(),
            image: $('#file-input').val(),
            comment: "",
            like: 0,
        }
        
        var type = "POST";
        var my_url = url;
        console.log(formData);
        if(formData.image.length > 0)
        {
        	$.ajax({
	            type: type,
	            url: my_url,
	            data: formData,
	            dataType: 'json',
	            success: function (data) {
	                console.log(data);
	                var post = '<div class="container peoplepost" style="padding-left: 0px; margin-left: 0px;padding-bottom: 20px"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img class="atr2 center-block"><h2 style="text-align: center">'+data.like+'</h2><img class="like center-block" src="{{asset('images/love.png')}}"></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10"><h5>{{Auth::user()->name}}<span style="float: right;"><b style="font-size: 0.8em;">2h</b></span></h5><p>'+data.post+'</p><img class="contohpost" src="images/'+data.image+'"><img class="comment" src="{{asset('images/chatx.png')}}"><span><img class="share" src="{{asset('images/share.png')}}"></span></div></div>';
	                    $('#post-list').prepend(post).hide();
	                    $('#post-list').fadeIn('slow');
	                    $('#post-data').val(" ");
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        });
        }
        else{
        	$.ajax({
	            type: type,
	            url: my_url,
	            data: formData,
	            dataType: 'json',
	            success: function (data) {
	                console.log(data);
	                var post = '<div class="container peoplepost" style="padding-left: 0px; margin-left: 0px;padding-bottom: 20px"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img class="atr2 center-block"><h2 style="text-align: center">'+data.like+'</h2><img class="like center-block" src="{{asset('images/love.png')}}"></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10"><h5>{{Auth::user()->name}}<span style="float: right;"><b style="font-size: 0.8em;">2h</b></span></h5><p>'+data.post+'</p><img class="comment" src="{{asset('images/chatx.png')}}"><span><img class="share" src="{{asset('images/share.png')}}"></span><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img class="atr2 center-block"></div><div class="col-xs-8 col-sm-9 col-md-9 col-lg-9"><input type="text" name="comment" id="comment-data" class="form-control comment-data" placeholder="Write your comment" required="required" title="" style="height: 50px;margin-left: 0;margin-right: 0px"></div><div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><span style="float:right;display: flex"><button style="background-color: transparent;border: none;float: right;outline: none;padding: 0;" id="comment-post" type="submit"><img src="images/chatx.png" class="comment-post" style=" margin-right: 15px; margin-bottom: 15px;margin-top: 20px;"></button></span></div></div></div></div>';
	                    $('#post-list').prepend(post).hide();
	                    $('#post-list').fadeIn('slow');
	                    $('#post-data').val(" ");
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        });
        }
        
    });
</script>
</body>
</html>