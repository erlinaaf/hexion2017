<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{'css/bootstrap.css'}}">
    <link rel="stylesheet" type="text/css" href="{{'css/register.css'}}">
    
    <title>Sign Up</title>

</head>
<body id="gradient">
<div class="container">
    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <h1 style="color:#999998">SIGN UP</h1>
        <h1 style="margin-top: -27px"><b>&nbsp&nbspSIGN UP</h1>
        <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
            <label for="fname" class="">Full Name</label>
            <input id="fname" type="text" class="form-control" name="fname" value="{{ old('fname') }}" required autofocus>

            @if ($errors->has('fname'))
                <span class="help-block">
                    <strong>{{ $errors->first('fname') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="">Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="">E-Mail Address</label>

            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="password-confirm" class="control-label">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>

        <button type="submit" class="btn btn-primary">SIGN UP</button>
    </form>
    <h5>Have an account?&nbsp<span style="color:#332F3E"><b>Log In</span></h5>
    <script src="{{'js/jquery.min.js'}}"></script>
    <script src="{{'js/background.js'}}"></script>
</div>
</body>
</html>