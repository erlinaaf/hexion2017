<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{'css/bootstrap.css'}}">
    <link rel="stylesheet" type="text/css" href="{{'css/login.css'}}">
    
    <title>Log In</title>

</head>
<body id="gradient">
<div class="container">
    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <h1 style="color:#999998" align="right">LOG IN</h1>
        <h1 style="margin-top: -27px" align="right"><b>LOG IN&nbsp&nbsp</h1>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="">Username</label>
            <input type="text" class="form-control" id="name" name="name">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="">Password</label>
            <input type="Password" class="form-control" id="password" name="password">
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">LOG IN</button>

        
    </form>
    <h5>Forgot password?</h5>
        <h5>Don't have an account?&nbsp<span style="color:#332F3E"><b>Sign Up</span></h5>
    <script src="{{'js/jquery.min.js'}}"></script>
    <script src="{{'js/background.js'}}"></script>
    
</script>
</div>
</body>
</html>