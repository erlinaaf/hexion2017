<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usergame extends Model
{
    protected $fillable = [
        'games_id',
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
