<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Usergame;
use App\Friend;
use App\Comment;
use Auth;
class TimelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);
        $users = User::with('friend')->get();
        // $users = User::leftJoin('friends','users.id','=','friends.user_id')
        //         ->select('*')->get();
        $users = DB::select('select * from users left join friends on users.id = friends.user_id');
        // dd($users);
        $achievements = $user->achievement()->get();
        $skills = $user->skill()->get();
        $games = Usergame::join('games','games.id','=','usergames.games_id')
                ->select('games.title')
                ->where('usergames.user_id',Auth::user()->id)
                ->get();
        $friends = Friend::with('user')->get();
        $commented = Comment::with('post')->with('user')->get();
        $cachievements = count($achievements);
        $cskills = count($skills);
        $cgames = count($games);
        $cfriends = count($friends)/2;
        $posts = Post::with('user')->with('comment')->get();
        return view('timeline',compact('users','posts','cachievements','cskills','cgames','friends','cfriends','commented'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
