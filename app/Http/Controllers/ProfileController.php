<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Game;
use App\Usergame;
use App\Post;
use App\Comment;
use App\Friend;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);
        $achievements = $user->achievement()->get();
        $skills = $user->skill()->get();
        $games = Usergame::join('games','games.id','=','usergames.games_id')
                ->select('games.title')
                ->where('usergames.user_id',Auth::user()->id)
                ->get();
        $posts = $user->post()->get();
        $commented = Comment::with('post')->with('user')->get();
        $friends = $user->friend()->get();
        // dd($commented);
        $lgames = Game::all();
        $cachievements = count($achievements);
        $cskills = count($skills);
        $cgames = count($games);
        $cfriends = count($friends);
        return view('user.profile',compact('user','achievements','skills','games','posts','friends','cachievements','cskills','cgames','cfriends','lgames','commented'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if(!empty($request['image'])){
            $post = $request->except('image');
            $post['image'] = $request->file('image')->getClientOriginalName();
            $request->file('image')->move('../resources/assets/images/post', $post['image']);
        }
        else{
            $post = $request->all();
        }
        $post = $user->post()->create($post);
        return response()->json($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if(!empty($request['image']))
        {
            $user = $request->except('image');
            $user['image'] = $request->file('image')->getClientOriginalName();
            $user['image'] = Auth::user()->id.'_'.Auth::user()->name.'.'.$this->substrAfter($request->file('image')         ->getClientOriginalName(),'.');
            $request->file('image')->move('../resources/assets/images/', $user['image']);
        }
        else{     
            $user = $request->all();
            $user['image'] = NULL;        
        }
        User::findOrfail($id)->update($user);
        return redirect('profile');
    }
    public function addGame(Request $request){
        $user = User::find(Auth::user()->id);
        $game = $user->game()->create($request->all());
        return redirect('/profile');
    }
    public function addAchievement(Request $request){
        $user = User::find(Auth::user()->id);
        $achievement = $user->achievement()->create($request->all());
        return redirect('/profile');
    }
    public function addSkill(Request $request){
        $user = User::find(Auth::user()->id);
        $skill = $user->skill()->create($request->all());
        return redirect('/profile');
    }
    public function addFriend(Request $request){
        Friend::create($request->all());
        return redirect('/home');
    }
    public function acceptFriend(Request $request,$id){
        Friend::find($id)->update($request->all());
        $accept['user_id'] = $request['friend_id'];
        $accept['friend_id'] = $request['user_id'];
        $accept['conf'] = 1;
        Friend::create($accept);
        return redirect('/home');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function comment(Request $request){
        $post = Post::find($request['post_id']);
        $comment = $post->comment()->create($request->all());
        return response()->json($comment);
    }
}
